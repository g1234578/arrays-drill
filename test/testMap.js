

const array = require('../array');
const map = require('../map');

function iteratee(element){
    return element*2;
}

function callBack(element){
    let resultElement = iteratee(element);
    return resultElement;
}

const result=map(array,callBack);

if(result != [] && result.length != 0){
    console.log(result);
}

else{
    
}