const array = require("../array");
const reduce = require("../reduce");

function callback(accumulator,element){
    return accumulator+element;
}

const result = reduce(array,callback,0);

if(result){
    console.log(result);
}
else{
    console.log("data is not found");
}
