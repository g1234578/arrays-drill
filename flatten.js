const nestedArray = [1, [2], [[3]], [[[4]]]]; // use this to test 'flatten'

function flatten(elements) {
  // Flattens a nested array (the nesting can be to any depth).
  // Hint: You can solve this using recursion.
  // Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];
  let flattenedArray = [];

  if (Array.isArray(elements)) {
    for (let element of elements) {
      if (Array.isArray(element)) {
        let nestedFlatten = flatten(element);
        for (let nestedElement of nestedFlatten) {
          flattenedArray.push(nestedElement);
        }
      } else {
        flattenedArray.push(element);
      }
    }

    return flattenedArray;
  }
  else{
    return [];
  }
}

module.exports = flatten;
