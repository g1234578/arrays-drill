function filter(array, cb) {
  let result = [];
  if (Array.isArray(array) && typeof(cb) === 'function') {
    for (let element of array) {
      let arr = cb(element);
      if (arr) {
        result.push(element);
      }
    }
    return result;
  }
  else{
    return [];
  }
}

module.exports = filter;
