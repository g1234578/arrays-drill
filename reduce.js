function reduce(elements,cb,startingValue){
    if(Array.isArray(elements) && typeof(cb) === 'function'){
        let accumulator = startingValue !== undefined ? startingValue : elements[0];

    for(let index of elements){
        accumulator = cb(accumulator,index);
    }

    return accumulator;
    }

    else{
        return undefined;
    }
}

module.exports = reduce;