function find(array, cb) {
  let result = undefined;
  if (Array.isArray(array) && typeof(cb) ==='function') {
    for (let index of array) {
      let arr = cb(index);
      if (arr) {
        result = index;
      }
    }
    return result;
  }
  else{
    return undefined;
  }
}

module.exports = find;
