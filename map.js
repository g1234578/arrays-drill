function map(array, cb) {
  let result = [];
  if(Array.isArray(array) && typeof(cb) === 'function'){
    for (let index of array) {
      let arr = cb(index);
      result.push(arr);
    }
    return result;
  }
  else{
    return [];
  }
}

module.exports = map;
